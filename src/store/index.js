import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    tasks: JSON.parse(localStorage.getItem('tasks') || '[]').map( task => {
      if (new Date(task.date) < new Date()) {
        task.status = 'outdated'
      }
      return task
    })
  },
  // Единственным способом изменения состояния хранилища во Vuex являются мутации. Мутации во Vuex очень похожи на события: каждая мутация имеет строковый тип и функцию-обработчик. В этом обработчике и происходят, собственно, изменения состояния, переданного в функцию первым аргументом:
  mutations: {
    createTask(state, task) {
      state.tasks.push(task)
      localStorage.setItem('tasks', JSON.stringify(state.tasks))
    },
    updateTask(state, {id, description, date}) {
      const tasks = state.tasks.concat() // concat() возвращает новый массив, состоящий из массива, на котором он был вызван, соединённого с другими массивами и/или значениями, переданными в качестве аргументов.
      const idx = tasks.findIndex(t => t.id === id)
      const task = tasks[idx]

      const status = new Date(date) > new Date() ? 'active' : 'outdated'

      tasks[idx] = {...task, date, description, status} // многоточие это Spread syntax позволяет расширить доступные для итерации элементы (например, массивы или строки) в местах для функций: где ожидаемое количество аргументов для вызовов функций равно нулю или больше нуля. для элементов (литералов массива). для выражений объектов: в местах, где количество пар "ключ-значение" должно быть равно нулю или больше (для объектных литералов). Этот объект будет содержать все поля, относящиеся к этой задаче и обновит только указанные поля
      
      // debugger

      state.tasks = tasks

      localStorage.setItem('tasks', JSON.stringify(state.tasks))
    },
    completeTask(state, id) {
      const idx = state.tasks.findIndex(t => t.id === id)
      
      state.tasks[idx].status = 'completed'
      
      localStorage.setItem('tasks', JSON.stringify(state.tasks))
    }
  },
  // Действия — похожи на мутации с несколькими отличиями:
  // Вместо того, чтобы напрямую менять состояние, действия инициируют мутации;
  // Действия могут использоваться для асинхронных операций.
  actions: {
    // Деструктуризация (destructuring assignment) – это особый синтаксис присваивания, при котором можно присвоить массив или объект сразу нескольким переменным, разбив его на части
    createTask({commit}, task) {
      commit('createTask', task) // вызывает мутацию ????
    },
    updateTask({commit}, task) {
      commit('updateTask', task)
    },
    completeTask({commit}, id) {
      commit('completeTask', id)
    }
  },
  modules: {
  },
  getters: {
    // возвращаю поле из state
    tasks: s => s.tasks,
    // ф-ия, кот- возвращает функцию
    taskById: s => id => s.tasks.find(t => t.id === id)
  }
})
